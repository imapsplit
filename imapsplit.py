#!/usr/bin/env python2.4

__version__ = "1.0RC1"
__author__ = "Nedko Arnaudov (mailto:nedko@arnaudov.name)"
__copyright__ = "(C) 2005 Nedko Arnaudov. GNU GPL 2."

print "===================="
print "imapsplit version %s %s" % (__version__, __copyright__)

#defaults
imap_servers = []
debug_fetch_parser = False
dump_fetch_result_flag = False
dump_header_flag = True

import getpass, imaplib, types, re, quopri, base64, email.Parser, email.Header, locale

out_charset = locale.getpreferredencoding()
if out_charset == None:
    out_charset = "utf-8"

#print("Using charset %s for stdout" % out_charset)

def printex(str):
    try:
        print str.encode(out_charset, "replace")
    except UnicodeDecodeError, e:
        print "cannot print: %s" % e
    except:
        print "cannot print"

def decode_rfc1252(data):
    debug = 0
    i = 0
    result = ""
    while i < len(data):
        if i + 1 < len(data) and data[i:i+2] == "=?":
            if debug >= 2: printex("Found =? mark")
            m = re.match(r"=\?([A-Za-z0-9-]+)\?([QqBb])\?([^?]+)\?=", data[i:])
            if m != None:
                if debug >= 1: printex("----------------------------------")
                if debug >= 1: printex("Found rfc1522 \"encoded-word\"")
                if debug >= 1: printex("Charset: %s" % m.group(1))
                if m.group(2) == "B" or m.group(2) == "b":
                    if debug >= 1: printex("Encoding: BASE64")
                    if debug >= 1: printex("BASE64 data: \"%s\"" % m.group(3))
                    decoded_data = base64.b64decode(m.group(3))
                    if debug >= 2:
                        printex("Decoded data:")
                        printex("----------------")
                        for c in decoded_data:
                            printex("0x%X" % ord(c))
                        printex("----------------")
                    decoded_data_string = unicode(decoded_data, m.group(1))
                    if debug >= 2:
                        printex("Object: %s (%s)" % (repr(decoded_data), repr(type(decoded_data))))
                        printex("Object: %s (%s)" % (repr(decoded_data_string), repr(type(decoded_data_string))))
                    if debug >= 1: printex("Decoded data: \"%s\"" % decoded_data_string)
                    result += decoded_data_string
                elif m.group(2) == "Q" or m.group(2) == "q":
                    if debug >= 1: printex("Encoding: Quoted-Printable")
                    if debug >= 1: printex("Quoted-Printable data: \"%s\"" % m.group(3))
                    decoded_data = quopri.decodestring(m.group(3), True)
                    if debug >= 2:
                        printex("Decoded data:")
                        printex("----------------")
                        for c in decoded_data:
                            printex("0x%X" % ord(c))
                        printex("----------------")
                    if debug >= 1: printex("Decoded data: \"%s\"" % unicode(decoded_data, m.group(1)))
                    decoded_data_string = unicode(decoded_data, m.group(1))
                    if debug >= 2:
                        printex("Object: %s (%s)" % (repr(decoded_data), repr(type(decoded_data))))
                        printex("Object: %s (%s)" % (repr(decoded_data_string), repr(type(decoded_data_string))))
                    if debug >= 1: printex("Decoded data: \"%s\"" % decoded_data_string)
                    result += decoded_data_string
                else:
                    printex(u"Unknown encoding \"%s\"" % m.group(2))
                    sys.exit(1)
                i += 2 + len(m.group(1)) + 1 + len(m.group(2)) + 1 + len(m.group(3)) + 2
                if debug >= 1: printex("----------------------------------")
                continue
        result += data[i].encode('utf-8', 'replace')
        i += 1
    return result

def parse_imap(data):
    result = []
    if debug_fetch_parser: printex("Parsing %s" % repr(data))
    i = 0
    while i < len(data):
        if debug_fetch_parser: printex("%u" % i)
        if data[i].isspace():
            if debug_fetch_parser: printex("Ignoring whitespace")
            i += 1
        elif data[i].isdigit():
            if debug_fetch_parser: printex("Found number")
            m = re.match(r"(\d+)", data[i:])
            result.append(('number', m.group(1)))
            i += len(m.group(1))
        elif data[i] == "{":
            if debug_fetch_parser: printex("Found string literal")
            m = re.match(r"{(\d+)}", data[i:])
            i += len(m.group(1))+2
            size = int(m.group(1))
            result.append(('string', data[i:i+size]))
            i += size
        elif data[i] == "\"":
            if debug_fetch_parser: printex("Found quoted string")
            m = re.match(r"\"([^\"]+)\"", data[i:])
            result.append(('string', m.group(1)))
            i += len(m.group(1))+2
        elif data[i] == "(":
            if debug_fetch_parser: printex("Found list")
            j = i+1
            nest = 0
            while j < len(data):
                if data[j] == ")":
                    if (nest == 0):
                        break
                    nest -= 1
                elif data[j] == "(":
                    nest += 1
                elif data[j] == "{":
                    m = re.match(r"{(\d+)}", data[j:])
                    j += len(m.group(1))+2
                    j += int(m.group(1))
                    continue
                j += 1
            result.append(('list', parse_imap(data[i+1:j])))
            i = j+1
        elif data[i] == "\\":
            if debug_fetch_parser: printex("Found flag")
            m = re.match(r"([A-Za-z]+)", data[i+1:])
            result.append(('flag', m.group(1)))
            i += len(m.group(1))+1
        else:
            if debug_fetch_parser: printex("Found identifier")
            m = re.match(r"([A-Za-z0-9_.-]+)", data[i:])
            result.append(('identifier', m.group(1)))
            i += len(m.group(1))
    return result

def parse_fetch_response(data):
    s = ""
    for part in data:
        if (type(part) == types.StringType or type(part) == types.UnicodeType):
            s += part
        elif (type(part) == types.TupleType):
            for tuple_part in part:
                s += tuple_part
        else:
            return None

    stage1 = parse_imap(s)

#    printex('------- stage1 --------')
#    printex("%s" % (repr(stage1)))
#    printex('-----------------------')

    result = {}
    if (stage1[0][0] != 'number'):
        return None
    result['sequence_number'] = stage1[0][1]
    if (stage1[1][0] != 'list'):
        return None
    stage2 = stage1[1][1]
    i = 0
    while i+1 < len(stage2):
#        printex("%s" % repr(stage2[i]))
        if stage2[i][0] != 'identifier':
            return None
#        printex("%s" % repr(stage2[i+1]))
        if stage2[i][1] == 'FLAGS':
            result[stage2[i][1]] = []
            for f in stage2[i+1][1]:
                if f[0] != 'flag':
                    return None
                result[stage2[i][1]].append(f[1])
        else:
            result[stage2[i][1]] = stage2[i+1][1]
        i += 2
    return result

def print_sequence_number(result):
    if result.has_key('sequence_number'):
        printex("sequence_number: %s" % result['sequence_number'])

def print_uid(result):
    if result.has_key('UID'):
        printex("UID: %s" % result['UID'])

def print_flags(result):
    if result.has_key('FLAGS'):
        printex("FLAGS: %s" % repr(result['FLAGS']))

def print_internal_date(result):
    if result.has_key('INTERNALDATE'):
        printex("INTERNALDATE: %s" % result['INTERNALDATE'])

def print_header(result):
    if result.has_key('RFC822.HEADER'):
        printex('Header:')
        printex('-----------------------')
        printex(result['RFC822.HEADER'])
        printex('-----------------------')

def print_body(result):
    if result.has_key('RFC822.TEXT'):
        printex('Body:')
        printex('-----------------------')
        printex(result['RFC822.TEXT'])
        printex('-----------------------')

def dump_fetch_result(result):
    printex('-----------------------')
    for k,v in result.iteritems():
        printex("'%s': '%s'" % (k,repr(v)))
    printex('-----------------------')

    print_sequence_number(result)
    print_uid(result)
    print_flags(result)
    print_internal_date(result)
    print_header(result)
    print_body(result)
    return

def unfold_rfc822(data):
    result = ""
    i = 0
    while i < len(data):
        if i + 2 < len(data) and data[i] == '\r' and data[i+1] == '\n' and (data[i+2] == ' ' or data[i+2] == '\t'):
            i += 3
            while data[i] == ' ' or data[i] == '\t':
                i += 1
            continue
        result += data[i]
        i += 1
    return result

def dump_header(header):
    msg = email.Parser.Parser().parsestr(header)
    if msg.has_key("Subject"):
        field = msg.get("Subject")
        field = unfold_rfc822(field)
        field = decode_rfc1252(field)
        printex("Subject: \"%s\"" % field)
    if msg.has_key("From"):
        field = msg.get("From")
        field = unfold_rfc822(field)
        field = decode_rfc1252(field)
        printex("From: %s" % field)
    if msg.has_key("List-Id"):
        field = msg.get("List-Id")
        field = unfold_rfc822(field)
        field = decode_rfc1252(field)
        printex("List-Id: \"%s\"" % field)

def fetch_header(uid):
    printex('... Fetching header of message with UID %s ...' % uid)
#    fetch_request = '(RFC822.SIZE INTERNALDATE FLAGS RFC822.HEADER RFC822.TEXT)'
    fetch_request = '(RFC822.HEADER)'
    status, data = M.uid('fetch', uid, fetch_request)
    if status != "OK":
        printex('Cannot fetch message with UID %s [%s]' % (uid, status))
        return None

    result = parse_fetch_response(data)
    if result == None:
        printex('Cannot parse fetch command response for message with UID %s' % uid)
        return None

    if dump_fetch_result_flag: dump_fetch_result(result)

    if not result.has_key('RFC822.HEADER'):
        printex('Server has not returned header we requested for message with UID %s' % uid)
        return None

    if dump_header_flag:
        try:
            dump_header(result['RFC822.HEADER'])
        except:
            printex("Cannot dump header")

    return result['RFC822.HEADER']

def find_target(uid):
    header = None

    index = 0
    for rule in split_rules:
#        printex("Checking rule %u" % index)
        if not rule.has_key('target'):
            printex('Skipping rule %u because it has no target' % index)
            return None
        if not rule.has_key('match'):
            printex('Skipping rule %u because it has no match method' % index)
            return None
        if rule['match'] == 'always':
            return rule['target']

        if not rule.has_key('match_regexp'):
            printex('Skipping rule %u because it has no match regexp' % index)
            return None
        if rule['match'] == 'header':
            if header == None:
                header = fetch_header(uid)
                if header == None:
                    printex("Ignoring message with UID %s because cannot fetch its header from server" % uid)
                    return None

            m = re.search(rule['match_regexp'], header.replace('\r\n', '\n'), re.MULTILINE)
            if m != None:
                return rule['target']
        else:
            printex('Skipping rule %u because of unknown match method' % index)
            return None
        index += 1

def move_message(uid, target):
    printex('Moving message with UID %s to %s ...' % (uid, target))
    if disable_move: return
    printex('Copying message with UID %s to %s ...' % (uid, target))
    status, data = M.uid('copy', uid, target)
    if status != "OK":
        printex('Cannot copy message with UID %s to %s [%s]' % (uid, target, status + " " + data[0]))
        if status == 'NO' and re.match(r"^\[TRYCREATE\]", data[0]) != None:
            printex('Trying to create mailbox %s ...' % target)
            status, data = M.create(target)
            if status != "OK":
                printex('Cannot create mailbox %s [%s]' % (target, status + " " + data[0]))
            printex('Retried copy of message with UID %s to %s ...' % (uid, target))
            status, data = M.uid('copy', uid, target)
            if status != "OK":
                printex('Cannot copy message with UID %s to %s [%s]' % (uid, target, status + " " + data[0]))
                return False
        else:
            return False
    printex('Marking message with UID %s as deleted ...' % uid)
    status, data = M.uid('store', uid, '+FLAGS', '(\Deleted)')
    if status != "OK":
        printex('Cannot set deleted flag to message with UID %s [%s]' % (uid, status))
        return False
    return True

# Read config file
import sys, os
old_path = sys.path
sys.path = [os.environ['HOME'] + "/.imapsplit"]
from config import *
sys.path = old_path

disable_move = False

for server in imap_servers:
    printex("-------")
    split_rules = server['split_rules']

    deleted_count = 0

    hostname = server['host'];
    user = server['user'];
    if server['type'] == 'imaps':
        port = 993
        if server.has_key('port'):
            port = server['port']
        M = imaplib.IMAP4_SSL(hostname, port)
    elif server['type'] == 'imap':
        port = 143
        if server.has_key('port'):
            port = server['port']
        M = imaplib.IMAP4(hostname, port)
    else:
        printex("Unknown server type \"%s\"" % server['type'])
        continue

    printex("Splitting on %s://%s@%s:%u :" % (server['type'], user, hostname, port))

#    printex("Protocol: " + M.PROTOCOL_VERSION)

    if server.has_key('password'):
        password = server['password']
    else:
        password = getpass.getpass("Password for %s://%s@%s:%u : " % (server['type'], user, hostname, port))
    M.login(user, password)

    M.select()
    status, data = M.uid('search', None, 'ALL')

    if len(data[0].split()) == 0:
        printex("no new messages")
    else:    
        printex("%u new message(s)" % len(data[0].split()))

    for uid in data[0].split():
        printex('==========================================================================================')
        printex('... Processing message with UID %s ...' % uid)
        target = find_target(uid)
        if (target == None):
            printex('Ignoring message with UID %s because no split rule matched.' % uid)
        else:
            if move_message(uid, target):
                deleted_count += 1

    if len(data[0].split()) != 0:
        printex('==========================================================================================')

    if deleted_count > 0:
        printex('Expunging ...')
        M.expunge()

    M.close()

    M.logout()

# imap_servers is array of dictionaries, with one dictionary per server
# These keys are defined for server dictionary:
#   'type' - REQUIRED, Protocol to use. 'imap' and 'imaps' are the valid values .
#   'host' - REQUIRED, hostname of the server.
#   'port' - OPTIONAL, port of the server. Defaults to 143 for IMAP and for 993 for IMAPS
#   'user' - REQUIRED, username at the server
#   'password' - OPTIONAL, password for the username at the server
#   'split_rules' - REQUIRED, array of rule dictionaries, with one dictionary per rule
#
# These keys are defined for rule dictionary:
#   'target' - REQUIRED, name of the IMAP mailbox where message will be moved if matches the rule
#   'match' - REQUIRED, match method. 'header' and 'always' are the valid values.
#             'header' means to match mesage header against the regular expression specified in 'match_regexp' key value.
#             'always' means to always match the rule. Useful as last rule.
#   'match_regexp' - REQUIRED if 'match' key has 'header' value, is the regular expresion used to match. See 'match' key.
#
# The array of rules (rule dictionaries) is iterated from first to last, until rule matches, then message is
# moved to IMAP mailbox specified in the 'target' key of the matched rule dictionary.
# If no rule matches, message is left untouched.

imap_servers = [
    # IMAPS server at somewhere.com
    {'type': 'imaps',
     'host': 'somewhere.com',
     'user': 'chochko',
     'password': 'mochko',
     'split_rules': [{'target': 'ros-dev',
                      'match': 'header',
                      'match_regexp': r"^List-Id:  *ReactOS Development List  *<ros-dev\.reactos\.com>$"
                      },
                     {'target': 'miscellaneous',
                      'match': 'always'
                      }
                     ]
     },
    # IMAP server accessible at localhost:1430 by using SSH tunnel (SSH tunnel is stared externaly)
    {'type': 'imap',
     'host': 'localhost',
     'port': 1430,
     'user': 'smarty',
     'split_rules': [{'target': 'miscellaneous',
                      'match': 'always'
                      }
                     ]
     }
    ]

debug_fetch_parser = False
dump_fetch_result_flag = False
dump_header_flag = True
